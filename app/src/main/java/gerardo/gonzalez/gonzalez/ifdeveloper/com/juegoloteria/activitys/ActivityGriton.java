package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.activitys;

import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.truizlop.fabreveallayout.FABRevealLayout;
import com.truizlop.fabreveallayout.OnRevealChangeListener;

import java.util.Random;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogConfirmarSalida;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogJuegoTerminadoGriton;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.interfaces.TarjetasTermiadas;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class ActivityGriton extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout ll;
    private RelativeLayout scrollView;
    private ImageButton btnPlay, btnResultados, btnSiguiente;
    private ImageView imageView, reversoTarjeta, cerrar;
    private CardView contenedor;
    private ImageView res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12, res13, res14,
            res15, res16, res17, res18, res19, res20, res21, res22, res23, res24, res25, res26, res27, res28,
            res29, res30, res31, res32, res33, res34, res35, res36, res37, res38, res39, res40, res41, res42,
            res43, res44, res45, res46, res47, res48, res49, res50, res51, res52, res53, res54;
    private static MediaPlayer mp;
    private SoundPool soundPool;
    private int ok = 0, id, index;
    private Random random;
    private static boolean comparador;
    private static int contador;
    private ImageView resultados[];
    private int audios[], tarjetas[];
    private boolean basura[];
    private ImageButton restart;
    private SoundPool sp, soundnext, soundBarajear;
    private int playSound, playNext, playBaraja;
    private Animation cambio_tarjeta1, cambio_tarjeta2, leftInt, leftOut, fadeIn;
    private FABRevealLayout fabRevealLayout;
    private TextView txtTituloDescripcion, txtDescripcion, txtTituloHistorial;
    private Handler handler;
    private DialogJuegoTerminadoGriton dialog;
   // private TarjetasTermiadas tarjetasTermiadas;
    private View.OnClickListener onClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_griton);

        playSound               = 0;
        playNext                = 0;
        handler                 = new Handler();
        comparador              = false;
        contador                = 0;
        id                      = 0;
        index                   = 0;
        random                  = new Random();

        dialog                  = new DialogJuegoTerminadoGriton();
        txtTituloDescripcion    = findViewById(R.id.txt_titulo_descripcion_griton);
        txtDescripcion          = findViewById(R.id.txt_descripcion_griton);
        txtTituloHistorial      = findViewById(R.id.txt_titulo_historial);

        txtTituloHistorial.setTypeface(Utils.fuentesita(this));
        txtTituloDescripcion.setTypeface(Utils.fuentesita(this));
        txtDescripcion.setTypeface(Utils.fuentesita(this));

        //SOUNDPOOL
        sp                      = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        playSound               = sp.load(getApplicationContext(), R.raw.soundboton, 1);
        soundnext               = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        playNext                = soundnext.load(getApplicationContext(), R.raw.nextcard, 1);
        soundBarajear           = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        playBaraja              = soundBarajear.load(getApplicationContext(), R.raw.barajear, 1);
        //ANIMACION
        cambio_tarjeta1         = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slider);
        cambio_tarjeta2         = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        leftInt                 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_in);
        leftOut                 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_out);
        fadeIn                  = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

        CastingDatos();

        contenedor              = findViewById(R.id.tarjeta_contenedor);
        imageView               = findViewById(R.id.tarjeta);
        ll                      = findViewById(R.id.llContenedor);
        reversoTarjeta          = findViewById(R.id.reverso_tarjeta);
        scrollView              = findViewById(R.id.resulScroll);
        //Botones
        btnPlay                 = findViewById(R.id.play);
        restart                 = findViewById(R.id.restart);
        btnResultados           = findViewById(R.id.historial);
        btnSiguiente            = findViewById(R.id.siguiente);
        cerrar                  = findViewById(R.id.cerrar);
        //SOUNDPOOL
        soundPool               = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        ok                      = soundPool.load(getApplicationContext(), R.raw.ninosgritando, 1);

        //FAB-REVEAL-LAYOUT
        fabRevealLayout         = findViewById(R.id.fab_reveal_layout);
        configureFABReveal(fabRevealLayout);

        btnPlay.setOnClickListener(this);
        btnPlay.setClickable(false);
        restart.setOnClickListener(this);
        restart.setClickable(false);
        cerrar.setOnClickListener(this);
        btnResultados.setOnClickListener(this);
        btnResultados.setClickable(false);
        btnSiguiente.setOnClickListener(this);

        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll.startAnimation(cambio_tarjeta1);
                ll.setVisibility(View.INVISIBLE);
                scrollView.startAnimation(cambio_tarjeta2);
                cerrar.startAnimation(cambio_tarjeta2);
                scrollView.setVisibility(View.VISIBLE);
                cerrar.setVisibility(View.VISIBLE);
                if (playSound != 0) {
                    sp.play(playSound, 1, 1, 0, 0, 1);
                }
            }
        };

        //Arreglos
        basura = new boolean[54];
        tarjetas = new int[]{
                R.drawable.alacran, R.drawable.apache, R.drawable.arana, R.drawable.arbol, R.drawable.arpa
                , R.drawable.bandera, R.drawable.bandolon, R.drawable.barril, R.drawable.borracho, R.drawable.bota
                , R.drawable.botella, R.drawable.calavera, R.drawable.camaron, R.drawable.campana, R.drawable.cantarito
                , R.drawable.catrin, R.drawable.cazo, R.drawable.chalupa, R.drawable.corazon, R.drawable.corona
                , R.drawable.cotorro, R.drawable.dama, R.drawable.diablito, R.drawable.escalera, R.drawable.estrella
                , R.drawable.garza, R.drawable.gorrito, R.drawable.jaras, R.drawable.luna, R.drawable.maceta
                , R.drawable.mano, R.drawable.melon, R.drawable.muerte, R.drawable.mundo, R.drawable.musico
                , R.drawable.negrito, R.drawable.nopal, R.drawable.pajaro, R.drawable.palma, R.drawable.paraguas
                , R.drawable.pera, R.drawable.pescado, R.drawable.pino, R.drawable.portada, R.drawable.rana
                , R.drawable.rosa, R.drawable.sandia, R.drawable.sirena, R.drawable.sol, R.drawable.soldado
                , R.drawable.tambor, R.drawable.valiente, R.drawable.venado, R.drawable.violoncello};

        audios = new int[]{
                R.raw.alacran, R.raw.apache, R.raw.arana, R.raw.arbol, R.raw.arpa
                , R.raw.bandera, R.raw.bandolon, R.raw.barril, R.raw.borracho, R.raw.bota
                , R.raw.botella, R.raw.calavera, R.raw.camaron, R.raw.campana, R.raw.cantarito
                , R.raw.catrin, R.raw.cazo, R.raw.chalupa, R.raw.corazon, R.raw.corona
                , R.raw.cotorro, R.raw.dama, R.raw.diablito, R.raw.escalera, R.raw.estrella
                , R.raw.garza, R.raw.gorrito, R.raw.jaras, R.raw.luna, R.raw.maceta
                , R.raw.mano, R.raw.melon, R.raw.muerte, R.raw.mundo, R.raw.musico
                , R.raw.negrito, R.raw.nopal, R.raw.pajaro, R.raw.palma, R.raw.paraguas
                , R.raw.pera, R.raw.pescado, R.raw.pino, R.raw.gallo, R.raw.rana
                , R.raw.rosa, R.raw.sandia, R.raw.sirena, R.raw.sol, R.raw.soldado
                , R.raw.tambor, R.raw.valiente, R.raw.venado, R.raw.violoncello};

        resultados = new ImageView[]{
                res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12, res13, res14, res15, res16, res17
                , res18, res19, res20, res21, res22, res23, res24, res25, res26, res27, res28, res29, res30, res31, res32
                , res33, res34, res35, res36, res37, res38, res39, res40, res41, res42, res43, res44, res45, res46, res47
                , res48, res49, res50, res51, res52, res53, res54};
    }

    private void configureFABReveal(FABRevealLayout fabRevealLayout) {
        fabRevealLayout.setOnRevealChangeListener(new OnRevealChangeListener() {
            @Override
            public void onMainViewAppeared(FABRevealLayout fabRevealLayout, View mainView) {
            }

            @Override
            public void onSecondaryViewAppeared(final FABRevealLayout fabRevealLayout, View secondaryView) {
                prepareBackTransition(fabRevealLayout);
            }
        });
    }

    private void prepareBackTransition(final FABRevealLayout fabRevealLayout) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                barajaTarjetas();
            }
        }, 1500);
    }

    private void CastingDatos() {
        res1 = findViewById(R.id.res1);
        res2 = findViewById(R.id.res2);
        res3 = findViewById(R.id.res3);
        res4 = findViewById(R.id.res4);
        res5 = findViewById(R.id.res5);
        res6 = findViewById(R.id.res6);
        res7 = findViewById(R.id.res7);
        res8 = findViewById(R.id.res8);
        res9 = findViewById(R.id.res9);
        res10 = findViewById(R.id.res10);
        res11 = findViewById(R.id.res11);
        res12 = findViewById(R.id.res12);
        res13 = findViewById(R.id.res13);
        res14 = findViewById(R.id.res14);
        res15 = findViewById(R.id.res15);
        res16 = findViewById(R.id.res16);
        res17 = findViewById(R.id.res17);
        res18 = findViewById(R.id.res18);
        res19 = findViewById(R.id.res19);
        res20 = findViewById(R.id.res20);
        res21 = findViewById(R.id.res21);
        res22 = findViewById(R.id.res22);
        res23 = findViewById(R.id.res23);
        res24 = findViewById(R.id.res24);
        res25 = findViewById(R.id.res25);
        res26 = findViewById(R.id.res26);
        res27 = findViewById(R.id.res27);
        res28 = findViewById(R.id.res28);
        res29 = findViewById(R.id.res29);
        res30 = findViewById(R.id.res30);
        res31 = findViewById(R.id.res31);
        res32 = findViewById(R.id.res32);
        res33 = findViewById(R.id.res33);
        res34 = findViewById(R.id.res34);
        res35 = findViewById(R.id.res35);
        res36 = findViewById(R.id.res36);
        res37 = findViewById(R.id.res37);
        res38 = findViewById(R.id.res38);
        res39 = findViewById(R.id.res39);
        res40 = findViewById(R.id.res40);
        res41 = findViewById(R.id.res41);
        res42 = findViewById(R.id.res42);
        res43 = findViewById(R.id.res43);
        res44 = findViewById(R.id.res44);
        res45 = findViewById(R.id.res45);
        res46 = findViewById(R.id.res46);
        res47 = findViewById(R.id.res47);
        res48 = findViewById(R.id.res48);
        res49 = findViewById(R.id.res49);
        res50 = findViewById(R.id.res50);
        res51 = findViewById(R.id.res51);
        res52 = findViewById(R.id.res52);
        res53 = findViewById(R.id.res53);
        res54 = findViewById(R.id.res54);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.siguiente:
                barajaTarjetas();
                break;
            case R.id.restart:
                if (playSound != 0) {
                    sp.play(playSound, 1, 1, 0, 0, 1);
                }
                for (int i = 0; i <= 53; i++) {
                    basura[i] = false;
                    resultados[i].setImageResource(R.drawable.imagen_transparente);
                    resultados[i].setVisibility(View.GONE);
                }
                if (mp != null) {
                    mp.release();
                    mp = null;
                }
                contador = 0;
                id = 0;
                index = 0;
                comparador = false;
                btnPlay.setImageResource(R.drawable.btn_play_dos_);
                btnPlay.setClickable(false);
                btnResultados.setClickable(false);
                restart.setClickable(false);
                reversoTarjeta.setVisibility(View.VISIBLE);
                reversoTarjeta.setAnimation(cambio_tarjeta2);
                imageView.setVisibility(View.INVISIBLE);
                imageView.setAnimation(cambio_tarjeta1);
                fabRevealLayout.revealMainView();
                break;
            case R.id.historial:
                if (playSound != 0) {
                    sp.play(playSound, 1, 1, 0, 0, 1);
                }
                ll.startAnimation(cambio_tarjeta1);
                ll.setVisibility(View.INVISIBLE);
                scrollView.startAnimation(cambio_tarjeta2);
                cerrar.startAnimation(cambio_tarjeta2);
                scrollView.setVisibility(View.VISIBLE);
                cerrar.setVisibility(View.VISIBLE);
                break;
            case R.id.cerrar:
                if (playSound != 0) {
                    sp.play(playSound, 1, 1, 0, 0, 1);
                }
                scrollView.startAnimation(cambio_tarjeta1);
                cerrar.startAnimation(cambio_tarjeta1);
                scrollView.setVisibility(View.INVISIBLE);
                cerrar.setVisibility(View.INVISIBLE);
                ll.startAnimation(cambio_tarjeta2);
                ll.setVisibility(View.VISIBLE);
                break;
            case R.id.play:
                if (playSound != 0) {
                    sp.play(playSound, 1, 1, 0, 0, 1);
                }
                if (mp.isPlaying()) {
                    mp.pause();
                    btnPlay.setImageResource(R.drawable.btn_play_dos_);
                } else {
                    mp.start();
                    btnPlay.setImageResource(R.drawable.btn_pause);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnPlay.setImageResource(R.drawable.btn_play_dos_);
                        }
                    }, mp.getDuration());
                }
                break;
        }
    }

    private void barajaTarjetas() {
        index = random.nextInt(54); // index, random, comparador, basura, contador, resultados, tarjetas, audios
        if (!comparador) { // si es la primera vez que se pulsa el boton        comparador == false
            comparador = true;
            imageView.setVisibility(View.VISIBLE);
            imageView.startAnimation(leftInt);
            reversoTarjeta.setVisibility(View.INVISIBLE);
            reversoTarjeta.startAnimation(leftOut);
            btnPlay.setClickable(true);
            btnResultados.setClickable(true);
            restart.setClickable(true);
            basura[index] = true;
            contador++;
            if (playBaraja != 0) {
                soundBarajear.play(playBaraja, 1, 1, 0, 0, 1);
            }
        } else if (!basura[index]) {
            basura[index] = true;
            contador++;
        } else {
            index = random.nextInt(54);
            while (basura[index] && contador < 54) {
                index = random.nextInt(54);
            }
            basura[index] = true;
            contador++;
        }
        if (contador <= 54) {
            contenedor.startAnimation(leftInt);
            imageView.setImageResource(tarjetas[index]);
            if (mp != null) {
                mp.release();
                btnPlay.setImageResource(R.drawable.btn_play_dos_);
            }
            if (playNext != 0) {
                soundnext.play(playNext, 1, 1, 0, 0, 1);
            }
            mp = MediaPlayer.create(getApplicationContext(), audios[index]);
            resultados[id].setImageResource(tarjetas[index]);
            resultados[id].setVisibility(View.VISIBLE);
            id++;
        } else {
            if (dialog != null){
                dialog.setTarjetasTermiadas(onClickListener);
                dialog.show(getSupportFragmentManager(), "tag");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mp != null) {
            mp.release();
            mp = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mp != null) {
            mp.stop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mp != null) {
            mp.start();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (playSound != 0) {
            sp.play(playSound, 1, 1, 0, 0, 1);
        }
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (scrollView.getVisibility() == View.VISIBLE) {
                scrollView.startAnimation(cambio_tarjeta1);
                cerrar.startAnimation(cambio_tarjeta1);
                scrollView.setVisibility(View.INVISIBLE);
                cerrar.setVisibility(View.INVISIBLE);
                ll.startAnimation(cambio_tarjeta2);
                ll.setVisibility(View.VISIBLE);
                return false;
            }else {
                DialogConfirmarSalida confirmarSalida = new DialogConfirmarSalida();
                confirmarSalida.show(getFragmentManager(), "tag");
                android.app.Fragment frag = getFragmentManager().findFragmentByTag("tag");
                if (frag != null) {
                    getFragmentManager().beginTransaction().remove(frag).commit();
                }
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}