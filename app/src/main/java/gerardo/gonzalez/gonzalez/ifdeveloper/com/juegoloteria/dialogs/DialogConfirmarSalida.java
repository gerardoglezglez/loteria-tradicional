package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.MainActivity;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class DialogConfirmarSalida extends DialogFragment implements View.OnClickListener{
    private View v;
    private ImageView fabAceptar, fabCerrar;
    private int playSound=0;
    private SoundPool sp;
    private TextView txtMensaje;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_confirmar_salida, container, false);
        sp              = new SoundPool(6, AudioManager.STREAM_MUSIC,0);
        playSound       = sp.load(getActivity(),R.raw.soundboton,1);
        fabAceptar      = v.findViewById(R.id.fab_aceptar);
        fabCerrar       = v.findViewById(R.id.fab_cerrar);
        txtMensaje      = v.findViewById(R.id.txt_mensaje_abandonar_juego);
        fabAceptar.setOnClickListener(this);
        fabCerrar.setOnClickListener(this);
        txtMensaje.setTypeface(Utils.fuentesita(getActivity()));

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_aceptar:
                dismiss();
                if (playSound!=0){sp.play(playSound,1,1,0,0,1);}
                Intent intent = new Intent(getActivity(),MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
            case R.id.fab_cerrar:
                dismiss();
                break;
        }
    }
}
