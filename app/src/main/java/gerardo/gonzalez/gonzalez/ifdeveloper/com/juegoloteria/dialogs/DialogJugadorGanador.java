package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class DialogJugadorGanador extends DialogFragment {
    private View v;
    private ImageView fabAceptar, avatarGanador;
    private int index;
    private SoundPool sp;
    private int[] arrayImage;
    TextView mensaje;

    public DialogJugadorGanador recibiendoMsj (int index){
        this.index = index;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_jugador_ganador, container, false);
        mensaje         = v.findViewById(R.id.txt_mensaje);
        avatarGanador   = v.findViewById(R.id.dialogo_ganador_completo_avatar);
        fabAceptar = v.findViewById(R.id.dia_ganador_fabaceptar);
        fabAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        arrayImage = new int[]{
                R.drawable.primer_avatar, R.drawable.segundo_avatar, R.drawable.tercer_avatar, R.drawable.cuato_avatar
        };
        avatarGanador.setImageResource(arrayImage[index]);
        mensaje.setTypeface(Utils.fuentesita(getActivity()));

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }
}
