package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.activitys;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.steamcrafted.loadtoast.LoadToast;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

import static gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R.id.juego_completo;

public class ActivityEscogerTablero extends AppCompatActivity implements View.OnClickListener {
    private ImageButton imagenAvatar1, imagenAvatar2, imagenAvatar3, imagenAvatar4, juegoLineas, juegoCompleto;
    private SoundPool soundPool;
    private int ok, tipo_juego_apuntador, a1, banderaUno, banderaDos;
    private Context context;
    private LoadToast lt;
    private RelativeLayout relativeParent;
    private ImageView  limpiar, btnComenzarJugar;;
    private TextView txtTipoTabla, txtAvatar;
    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.escoger_tablero);

        banderaUno      = 0;
        banderaDos      = 0;
        soundPool       = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        ok              = soundPool.load(getApplicationContext(), R.raw.sound_mariocoin, 1);
        btnComenzarJugar= findViewById(R.id.comenzar_juego);
        imagenAvatar1   = findViewById(R.id.primer_avatar);
        imagenAvatar2   = findViewById(R.id.segundo_avatar);
        imagenAvatar3   = findViewById(R.id.tercer_avatar);
        imagenAvatar4   = findViewById(R.id.cuarto_avatar);
        juegoLineas     = findViewById(R.id.juego_lineas);
        juegoCompleto   = findViewById(juego_completo);
        limpiar         = findViewById(R.id.limpiar);
        context         = getApplicationContext();
        relativeParent  = findViewById(R.id.relative_parent);
        txtAvatar       = findViewById(R.id.txt_tipo_juego);
        txtTipoTabla    = findViewById(R.id.txt_selecciona_avatar);

        txtAvatar.setTypeface(Utils.fuentesita(this));
        txtTipoTabla.setTypeface(Utils.fuentesita(this));
        imagenAvatar1.setOnClickListener(this);
        imagenAvatar2.setOnClickListener(this);
        imagenAvatar3.setOnClickListener(this);
        imagenAvatar4.setOnClickListener(this);
        juegoLineas.setOnClickListener(this);
        juegoCompleto.setOnClickListener(this);
        btnComenzarJugar.setOnClickListener(this);
        limpiar.setOnClickListener(this);

        lt = new LoadToast(this)
                .setProgressColor(Color.RED)
                .setText(getResources().getString(R.string.escoger_tablero_limpiando))
                .setTranslationY(100)
                .setBorderColor(Color.LTGRAY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.limpiar:
                lt.show();
                relativeParent.setBackgroundColor(getResources().getColor(R.color.background_dialod));
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lt.success();
                        relativeParent.setBackgroundColor(getResources().getColor(R.color.color_transparente));
                        juegoLineas.setImageResource(R.drawable.imagen_transparente);
                        juegoCompleto.setImageResource(R.drawable.imagen_transparente);
                        juegoLineas.setClickable(true);
                        juegoCompleto.setClickable(true);

                        imagenAvatar1.setImageResource(R.drawable.imagen_transparente);
                        imagenAvatar2.setImageResource(R.drawable.imagen_transparente);
                        imagenAvatar3.setImageResource(R.drawable.imagen_transparente);
                        imagenAvatar4.setImageResource(R.drawable.imagen_transparente);
                        imagenAvatar1.setClickable(true);
                        imagenAvatar2.setClickable(true);
                        imagenAvatar3.setClickable(true);
                        imagenAvatar4.setClickable(true);
                    }
                }, 1200);
                break;
            case R.id.primer_avatar:
                evaluandoClicks(0, imagenAvatar1);
                break;
            case R.id.segundo_avatar:
                evaluandoClicks(0, imagenAvatar2);
                break;
            case R.id.tercer_avatar:
                evaluandoClicks(0, imagenAvatar3);
                break;
            case R.id.cuarto_avatar:
                evaluandoClicks(0, imagenAvatar4);
                break;
            case R.id.juego_lineas:
                evaluandoClicks(1, juegoLineas);
                break;
            case juego_completo:
                evaluandoClicks(1, juegoCompleto);
                break;
            case R.id.comenzar_juego:
                elementosSeleccionados();
                if (banderaUno ==1 && banderaDos ==1){
                    Intent b = new Intent(getApplicationContext(), ActivityJugadorNormal.class);
                    b.putExtra("avatar", a1);
                    b.putExtra("tipo_juego_apuntador", tipo_juego_apuntador);
                    startActivity(b);
                }else {
                    Toast.makeText(context, getResources().getString(R.string.escoger_tablero_llena_datos), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void elementosSeleccionados() {
        if (imagenAvatar1.isClickable()) {
            a1 = 0;
            banderaUno=1;
        } else if (imagenAvatar2.isClickable()) {
            a1 = 1;
            banderaUno=1;
        } else if (imagenAvatar3.isClickable()) {
            a1 = 2;
            banderaUno=1;
        } else if (imagenAvatar4.isClickable()) {
            a1 = 3;
            banderaUno=1;
        }
        if (juegoLineas.isClickable()) {
            tipo_juego_apuntador = 0;
            banderaDos=1;
        } else if (juegoCompleto.isClickable()) {
            tipo_juego_apuntador = 1;
            banderaDos=1;
        }
    }

    private void evaluandoClicks(int index, ImageButton imageButton) {
        if (index == 0) {
            imagenAvatar1.setClickable(false);
            imagenAvatar2.setClickable(false);
            imagenAvatar3.setClickable(false);
            imagenAvatar4.setClickable(false);
            imageButton.setClickable(true);
        } else if (index == 1) {
            juegoCompleto.setClickable(false);
            juegoLineas.setClickable(false);
            imageButton.setClickable(true);
        }
        imageButton.setImageResource(R.drawable.comprobado);
        if (ok != 0) soundPool.play(ok, 1, 1, 0, 0, 1);
    }
}