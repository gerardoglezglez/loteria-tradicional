package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.interfaces.ParametrosDialogoGanador;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

/**
 * Created by Usuario on 27/04/2017.
 */

public class DialogConfirmacion extends DialogFragment implements View.OnClickListener, ParametrosDialogoGanador {
    private View v;
    private ImageView tarjetaUno, tarjetaDos, tarjetaTres, tarjetaCuatro,idAvatar, aceptar;
    private int playSound = 0, marcador, indexAvatar, a1,a2,a3,a4;
    private int[] arrayImage;
    private Drawable k, l, m, o;
    private SoundPool sp;
    private TextView txtMensaje;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_ganador, container, false);

        tarjetaUno      = v.findViewById(R.id.dialogo_tarjeta_uno);
        tarjetaDos      = v.findViewById(R.id.dialogo_tarjeta_dos);
        tarjetaTres     = v.findViewById(R.id.dialogo_tarjeta_tres);
        tarjetaCuatro   = v.findViewById(R.id.dialogo_tarjeta_cuatro);
        aceptar         = v.findViewById(R.id.dia_ganador_aceptar);
        sp              = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        playSound       = sp.load(getActivity(), R.raw.soundboton, 1);
        txtMensaje      = v.findViewById(R.id.txt_dialogo_ganador_mensaje);

        txtMensaje.setTypeface(Utils.fuentesita(getActivity()));
        aceptar.setOnClickListener(this);
        tarjetaUno.setImageDrawable(k);
        tarjetaDos.setImageDrawable(l);
        tarjetaTres.setImageDrawable(m);
        tarjetaCuatro.setImageDrawable(o);

        arrayImage = new int[]{
          R.drawable.primer_avatar, R.drawable.segundo_avatar, R.drawable.tercer_avatar, R.drawable.cuato_avatar
        };

        idAvatar = v.findViewById(R.id.dialogo_ganador_avatar);
        idAvatar.setImageResource(arrayImage[indexAvatar]);

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dia_ganador_aceptar:
                dismiss();
                break;
        }
    }

    @Override
    public void arregloDeGanador(int bandera, ImageView a, ImageView b, ImageView c, ImageView d, int invatar) {
        marcador = bandera;
        k = a.getDrawable();
        l = b.getDrawable();
        m = c.getDrawable();
        o = d.getDrawable();
        indexAvatar = invatar;
    }
}