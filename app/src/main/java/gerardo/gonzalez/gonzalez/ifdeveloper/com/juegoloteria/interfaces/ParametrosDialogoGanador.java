package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.interfaces;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

public interface ParametrosDialogoGanador {
    void arregloDeGanador(int bandera, ImageView img1, ImageView img2, ImageView img3, ImageView img4, int indexAvatar);
}
