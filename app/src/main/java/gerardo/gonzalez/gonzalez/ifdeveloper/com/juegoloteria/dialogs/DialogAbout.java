package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class DialogAbout extends DialogFragment {
    private TextView txtVersion, txtAcercaDe, txtAcercaDeTitulo, txtCreditos;
    private ImageButton btn_phone, btn_email;
    private View v;
    private Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.activity_about, container, false);

        context             = getActivity();

        txtVersion          = v.findViewById(R.id.acerca_de_autor);
        txtAcercaDe         = v.findViewById(R.id.acerca_de_datos);
        txtAcercaDeTitulo   = v.findViewById(R.id.acerca_de_creditos);
        txtCreditos         = v.findViewById(R.id.acerca_de_creditos_texto);

        btn_phone           = v.findViewById(R.id.btn_telefonodev);
        btn_email           = v.findViewById(R.id.btn_emaildev);

        txtVersion.setTypeface(Utils.fuentesita(context));
        txtAcercaDe.setTypeface(Utils.fuentesita(context));
        txtAcercaDeTitulo.setTypeface(Utils.fuentesita(context));
        txtCreditos.setTypeface(Utils.fuentesita(context));

        final String[] mail = new String[]{getResources().getString(R.string.email_correo)};
        final String telefono = getResources().getString(R.string.tel_numero);

        btn_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cell = new Intent(Intent.ACTION_VIEW);
                cell.setData(Uri.parse("tel:" + telefono));
                startActivity(cell);
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                Resources r = v.getResources();
                emailIntent.setData(Uri.parse("mailto: "));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, mail);
                emailIntent.setType("message/rfc822");
                try {
                    startActivity(Intent.createChooser(emailIntent, r.getString(R.string.titulo_enviarmail)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(v.getContext(), r.getString(R.string.sinapp_email), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }
}
