package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.activitys;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.steamcrafted.loadtoast.LoadToast;

import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogConfirmacion;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogConfirmarSalida;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogJugadorGanador;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.MainActivity;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;
import nl.dionsegijn.konfetti.KonfettiView;

public class ActivityJugadorNormal extends AppCompatActivity implements View.OnClickListener {
    private Animation parpadeo, girar;
    private ImageButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, btn14, btn15, btn16,
            ibtn1, ibtn2, ibtn3, ibtn4, ibtn5, ibtn6, ibtn7, ibtn8, ibtn9, ibtn10, ibtn11, ibtn12, ibtn13, ibtn14, ibtn15, ibtn16,
            imageButtons[], buttons[], multiImageButton[][], multiImage[][];
    private ImageView tipoJuego, tarjetaA, tarjetaB, tarjetaC, tarjetaD, tarjetasDeGanador[],
            limpiar, home, barajear, barajearPrimero, cerrarPrimero, barajearDos;
    private int apuntadorTipoJuego, id_tipo_juego, id_del_avatar;
    private static DialogConfirmacion dialogoDelGanador;
    private DialogJugadorGanador dialogJugadorGanador;
    private static FragmentManager fragmentManager;
    private SoundPool soundPool, sound_table, soundBoton, baraja;
    private int ok = 0, playTone, playBoton, soundBarajear;
    private KonfettiView konfettiView;
    private LoadToast lt;
    private RelativeLayout layoutParent, relContenedorTabla;
    private Handler handler;
    private CircleImageView idAvatar;
    private int tarjetas[];
    private int id, index, avatarUtilizado;
    private Random random;
    private static int contador, tope;
    private boolean basura[];
    private Drawable frijol;
    private Resources res;
    private int colorBackground, colorTransparent;
    private TextView tipoDeJuego, txtmuchaSuerte, txtBarajear;
    private LinearLayout llBotones, tablaDelJugador, llElementosBaraja;
    private DialogConfirmarSalida confirmarSalida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FULLSCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_jugador_normal);

        res                         = getResources();
        colorBackground             = res.getColor(R.color.fondo_tabla);
        colorTransparent            = res.getColor(R.color.color_transparente);
        contador                    = 0;
        id                          = 0;
        index                       = 0;
        tope                        = 0;
        random                      = new Random();
        frijol                      = getResources().getDrawable(R.drawable.frijol);
        txtBarajear                 = findViewById(R.id.jugador_normal_txt_barajear);
        llElementosBaraja           = findViewById(R.id.jugador_normal_llelementos_baraja);
        llBotones                   = findViewById(R.id.ll_juego_normal_botones);
        cerrarPrimero               = findViewById(R.id.jugador_normal_btn_cerrar);
        barajearPrimero             = findViewById(R.id.jugador_normal_btn_barajear);
        tablaDelJugador             = findViewById(R.id.tabla_del_jugador);
        relContenedorTabla          = findViewById(R.id.rel_contenedor_tabla);

        //ANIMACIONES
        parpadeo                    = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.parpadear);
        girar                       = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.girar);
        //SOUNDPOOL
        sound_table                 = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        soundPool                   = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        soundBoton                  = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        baraja                      = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);;

        playTone                    = sound_table.load(getApplicationContext(), R.raw.sound_tabla, 1);
        ok                          = soundPool.load(getApplicationContext(), R.raw.ninosgritando, 1);
        playBoton                   = soundBoton.load(getApplicationContext(), R.raw.soundboton, 1);
        soundBarajear               = baraja.load(getApplicationContext(), R.raw.barajear, 1);

        dialogoDelGanador           = new DialogConfirmacion();
        layoutParent                = findViewById(R.id.layout_parent);
        tipoJuego                   = findViewById(R.id.id_tipo_juego);
        konfettiView                = findViewById(R.id.konfettiView);
        fragmentManager             = getFragmentManager();
        Intent intent               = getIntent();
        handler                     = new Handler();
        confirmarSalida             = new DialogConfirmarSalida();
        idAvatar                    = findViewById(R.id.jugador_normal_avatar);
        barajear                    = findViewById(R.id.barajear);

        tarjetaA                    = new ImageView(this);
        tarjetaB                    = new ImageView(this);
        tarjetaC                    = new ImageView(this);
        tarjetaD                    = new ImageView(this);

        btn1                        = findViewById(R.id.boton1);
        btn2                        = findViewById(R.id.boton2);
        btn3                        = findViewById(R.id.boton3);
        btn4                        = findViewById(R.id.boton4);
        btn5                        = findViewById(R.id.boton5);
        btn6                        = findViewById(R.id.boton6);
        btn7                        = findViewById(R.id.boton7);
        btn8                        = findViewById(R.id.boton8);
        btn9                        = findViewById(R.id.boton9);
        btn10                       = findViewById(R.id.boton10);
        btn11                       = findViewById(R.id.boton11);
        btn12                       = findViewById(R.id.boton12);
        btn13                       = findViewById(R.id.boton13);
        btn14                       = findViewById(R.id.boton14);
        btn15                       = findViewById(R.id.boton15);
        btn16                       = findViewById(R.id.boton16);

        ibtn1                       = findViewById(R.id.ibutton1);
        ibtn2                       = findViewById(R.id.ibutton2);
        ibtn3                       = findViewById(R.id.ibutton3);
        ibtn4                       = findViewById(R.id.ibutton4);
        ibtn5                       = findViewById(R.id.ibutton5);
        ibtn6                       = findViewById(R.id.ibutton6);
        ibtn7                       = findViewById(R.id.ibutton7);
        ibtn8                       = findViewById(R.id.ibutton8);
        ibtn9                       = findViewById(R.id.ibutton9);
        ibtn10                      = findViewById(R.id.ibutton10);
        ibtn11                      = findViewById(R.id.ibutton11);
        ibtn12                      = findViewById(R.id.ibutton12);
        ibtn13                      = findViewById(R.id.ibutton13);
        ibtn14                      = findViewById(R.id.ibutton14);
        ibtn15                      = findViewById(R.id.ibutton15);
        ibtn16                      = findViewById(R.id.ibutton16);

        id_del_avatar               = intent.getExtras().getInt("avatar");
        apuntadorTipoJuego          = intent.getExtras().getInt("tipo_juego_apuntador");

        tipoDeJuego                 = findViewById(R.id.txt_tipo_juego);
        txtmuchaSuerte              = findViewById(R.id.txt_mucha_suerte);
        limpiar                     = findViewById(R.id.boton_limpiar);
        home                        = findViewById(R.id.boton_home);
        basura                      = new boolean[54];
        barajearDos                 = findViewById(R.id.jugador_normal_btn_barajear_dos);

        tipoDeJuego.setTypeface(Utils.fuentesita(this));
        txtmuchaSuerte.setTypeface(Utils.fuentesita(this));
        txtBarajear.setTypeface(Utils.fuentesita(this));
        limpiar.setOnClickListener(this);
        home.setOnClickListener(this);
        cerrarPrimero.setOnClickListener(this);
        barajearPrimero.setOnClickListener(this);
        barajear.setOnClickListener(this);
        barajearDos.setOnClickListener(this);

        if (id_del_avatar == 0) {
            idAvatar.setImageResource(R.drawable.primer_avatar);
            avatarUtilizado = 0;
        } else if (id_del_avatar == 1) {
            avatarUtilizado = 1;
            idAvatar.setImageResource(R.drawable.segundo_avatar);
        } else if (id_del_avatar == 2) {
            avatarUtilizado = 2;
            idAvatar.setImageResource(R.drawable.tercer_avatar);
        } else if (id_del_avatar == 3) {
            avatarUtilizado = 3;
            idAvatar.setImageResource(R.drawable.cuato_avatar);
        }

        if (apuntadorTipoJuego == 0) {
            tipoJuego.setImageDrawable(res.getDrawable(R.drawable.tabla_lineal_white));
            id_tipo_juego = 0;
        } else if (apuntadorTipoJuego == 1) {
            tipoJuego.setImageDrawable(res.getDrawable(R.drawable.tabla_lllena_white));
            id_tipo_juego = 1;
        }

        lt = new LoadToast(this)
                .setProgressColor(Color.RED)
                .setText(getResources().getString(R.string.escoger_tablero_limpiando))
                .setTranslationY(100)
                .setBorderColor(Color.LTGRAY);

        imageButtons = new ImageButton[]{
                ibtn1, ibtn2, ibtn3, ibtn4,
                ibtn5, ibtn6, ibtn7, ibtn8,
                ibtn9, ibtn10, ibtn11, ibtn12,
                ibtn13, ibtn14, ibtn15, ibtn16
        };
        buttons = new ImageButton[]{
                btn1, btn2, btn3, btn4,
                btn5, btn6, btn7, btn8,
                btn9, btn10, btn11, btn12,
                btn13, btn14, btn15, btn16
        };
        multiImage = new ImageButton[][]{
                {btn1, btn2, btn3, btn4},
                {btn5, btn6, btn7, btn8},
                {btn9, btn10, btn11, btn12},
                {btn13, btn14, btn15, btn16},

                {btn1, btn3, btn9, btn13},
                {btn2, btn6, btn10, btn14},
                {btn3, btn7, btn11, btn15},
                {btn4, btn8, btn12, btn16},

                {btn1, btn6, btn11, btn16},
                {btn4, btn7, btn10, btn13}
        };
        multiImageButton = new ImageButton[][]{
                {ibtn1, ibtn2, ibtn3, ibtn4},
                {ibtn5, ibtn6, ibtn7, ibtn8},
                {ibtn9, ibtn10, ibtn11, ibtn12},
                {ibtn13, ibtn14, ibtn15, ibtn16},

                {ibtn1, ibtn5, ibtn9, ibtn13},
                {ibtn2, ibtn6, ibtn10, ibtn14},
                {ibtn3, ibtn7, ibtn11, ibtn15},
                {ibtn4, ibtn8, ibtn12, ibtn16},

                {ibtn1, ibtn6, ibtn11, ibtn16},
                {ibtn4, ibtn7, ibtn10, ibtn13}
        };
        for (int i = 0; i <= 15; i++) {
            buttons[i].setOnClickListener(this);
            imageButtons[i].setOnClickListener(this);
            buttons[i].setClickable(false);
            imageButtons[i].setClickable(false);
        }

        tarjetas = new int[]{
                R.drawable.alacran, R.drawable.apache, R.drawable.arana, R.drawable.arbol, R.drawable.arpa
                , R.drawable.bandera, R.drawable.bandolon, R.drawable.barril, R.drawable.borracho, R.drawable.bota
                , R.drawable.botella, R.drawable.calavera, R.drawable.camaron, R.drawable.campana, R.drawable.cantarito
                , R.drawable.catrin, R.drawable.cazo, R.drawable.chalupa, R.drawable.corazon, R.drawable.corona
                , R.drawable.cotorro, R.drawable.dama, R.drawable.diablito, R.drawable.escalera, R.drawable.estrella
                , R.drawable.garza, R.drawable.gorrito, R.drawable.jaras, R.drawable.luna, R.drawable.maceta
                , R.drawable.mano, R.drawable.melon, R.drawable.muerte, R.drawable.mundo, R.drawable.musico
                , R.drawable.negrito, R.drawable.nopal, R.drawable.pajaro, R.drawable.palma, R.drawable.paraguas
                , R.drawable.pera, R.drawable.pescado, R.drawable.pino, R.drawable.portada, R.drawable.rana
                , R.drawable.rosa, R.drawable.sandia, R.drawable.sirena, R.drawable.sol, R.drawable.soldado
                , R.drawable.tambor, R.drawable.valiente, R.drawable.venado, R.drawable.violoncello
        };

        tarjetasDeGanador = new ImageView[]{
                tarjetaA, tarjetaB, tarjetaC, tarjetaD
        };
    }

    private void rellenandoTabla() {
        for (int g = 0; g < 16; g++) {
            index = random.nextInt(54);
            if (!basura[index]) {
                basura[index] = true;
                contador++;
            } else {
                index = random.nextInt(54);
                while (basura[index] && contador < 54) {
                    index = random.nextInt(54);
                }
                basura[index] = true;
                contador++;
            }
            if (contador <= 54 && tope < 16) {
                buttons[tope].setBackgroundResource(tarjetas[index]);
                id++;
                tope++;
            }
        }
        for (int g = 0; g < basura.length - 1; g++) {
            basura[g] = false;
        }
        contador    = 0;
        id          = 0;
        index       = 0;
        tope        = 0;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibutton1:
                comprobandoBoton(0);
                Ganador();
                break;
            case R.id.ibutton2:
                comprobandoBoton(1);
                Ganador();
                break;
            case R.id.ibutton3:
                comprobandoBoton(2);
                Ganador();
                break;
            case R.id.ibutton4:
                comprobandoBoton(3);
                Ganador();
                break;
            case R.id.ibutton5:
                comprobandoBoton(4);
                Ganador();
                break;
            case R.id.ibutton6:
                comprobandoBoton(5);
                Ganador();
                break;
            case R.id.ibutton7:
                comprobandoBoton(6);
                Ganador();
                break;
            case R.id.ibutton8:
                comprobandoBoton(7);
                Ganador();
                break;
            case R.id.ibutton9:
                comprobandoBoton(8);
                Ganador();
                break;
            case R.id.ibutton10:
                comprobandoBoton(9);
                Ganador();
                break;
            case R.id.ibutton11:
                comprobandoBoton(10);
                Ganador();
                break;
            case R.id.ibutton12:
                comprobandoBoton(11);
                Ganador();
                break;
            case R.id.ibutton13:
                comprobandoBoton(12);
                Ganador();
                break;
            case R.id.ibutton14:
                comprobandoBoton(13);
                Ganador();
                break;
            case R.id.ibutton15:
                comprobandoBoton(14);
                Ganador();
                break;
            case R.id.ibutton16:
                comprobandoBoton(15);
                Ganador();
                break;
            case R.id.boton_limpiar:
                limpiarTabla();
                break;
            case R.id.boton_home:
                if (playBoton != 0) soundBoton.play(playBoton, 1, 1, 0, 0, 1);
                startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                break;
            case R.id.jugador_normal_btn_cerrar:
                relContenedorTabla.setBackgroundColor(colorTransparent);
                llElementosBaraja.setVisibility(View.GONE);
                llBotones.setVisibility(View.VISIBLE);
                barajearDos.setVisibility(View.GONE);
                for (int i = 0; i <= 15; i++) {
                    buttons[i].setClickable(true);
                    imageButtons[i].setClickable(true);
                }
                break;
            case R.id.jugador_normal_btn_barajear:
                metodoBotonBarajear();
                break;
            case R.id.jugador_normal_btn_barajear_dos:
                barajearDos.setVisibility(View.GONE);
                metodoBotonBarajear();
                break;
        }
    }

    public void metodoBotonBarajear() {
        llElementosBaraja.setVisibility(View.VISIBLE);
        barajearPrimero.setVisibility(View.VISIBLE);
        relContenedorTabla.setBackgroundColor(colorTransparent);
        cerrarPrimero.setVisibility(View.GONE);
        tablaDelJugador.setVisibility(View.GONE);
        txtBarajear.setVisibility(View.GONE);
        barajearPrimero.startAnimation(girar);
        if (soundBarajear != 0) baraja.play(soundBarajear, 1, 1, 0, 0, 1);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rellenandoTabla();
                barajearPrimero.setVisibility(View.GONE);
                tablaDelJugador.setVisibility(View.VISIBLE);
                relContenedorTabla.setBackgroundColor(colorBackground);
                barajearPrimero.clearAnimation();
                cerrarPrimero.setVisibility(View.VISIBLE);
                barajearDos.setVisibility(View.VISIBLE);
                barajear.setVisibility(View.GONE);
            }
        }, 1000);
    }

    private void limpiarTabla() {
        lt.show();
        soundPool.stop(playTone);
        soundPool.stop(ok);
        konfettiView.reset();
        layoutParent.setBackgroundColor(getResources().getColor(R.color.background_dialod));
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                lt.success();
                for (int i = 0; i <= buttons.length - 1; i++) {
                    imageButtons[i].clearAnimation();
                    imageButtons[i].setBackgroundColor(res.getColor(R.color.color_transparente));
                    imageButtons[i].setImageResource(0);
                    imageButtons[i].setClickable(true);
                }
                layoutParent.setBackgroundColor(getResources().getColor(R.color.color_transparente));
                if (playBoton != 0) soundBoton.play(playBoton, 1, 1, 0, 0, 1);
            }
        }, 1000);
        tablaDelJugador.setBackgroundColor(colorTransparent);
        cerrarPrimero.setVisibility(View.GONE);
        tablaDelJugador.setVisibility(View.GONE);
        llBotones.setVisibility(View.GONE);
        txtBarajear.setVisibility(View.VISIBLE);
        llElementosBaraja.setVisibility(View.VISIBLE);
        barajearPrimero.setVisibility(View.VISIBLE);
        relContenedorTabla.setBackgroundColor(colorTransparent);
    }

    private void parametrosGanador(int index) {
        if (ok != 0) soundPool.play(ok, 1, 1, 0, 0, 1);
        for (int i = 0; i <= 3; i++) {
            tarjetasDeGanador[i].setImageDrawable(multiImage[index][i].getBackground());
            multiImageButton[index][i].setBackgroundColor(res.getColor(R.color.rojo_dos));
            multiImageButton[index][i].startAnimation(parpadeo);
        }
        for (int g = 0; g <= 15; g++) {
            imageButtons[g].setClickable(false);
        }
        Utils.Konfetty(konfettiView);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialogoDelGanador.arregloDeGanador(0, tarjetaA, tarjetaB, tarjetaC, tarjetaD, avatarUtilizado);
                dialogoDelGanador.show(fragmentManager, "Mensaje");
            }
        }, 2000);
    }

    private void comprobandoBoton(int indicador) {
        if (playTone != 0) sound_table.play(ok, 1, 1, 0, 0, 1);
        imageButtons[indicador].setImageDrawable(frijol);
        imageButtons[indicador].setBackgroundColor(res.getColor(R.color.bacnground_tablas));
    }

    private void Ganador() {
        if (id_tipo_juego == 0) {
            if (imageButtons[0].getDrawable() == frijol && imageButtons[1].getDrawable() == frijol && imageButtons[2].getDrawable() == frijol && imageButtons[3].getDrawable() == frijol) {
                parametrosGanador(0);
            } else if (imageButtons[4].getDrawable() == frijol && imageButtons[5].getDrawable() == frijol && imageButtons[6].getDrawable() == frijol && imageButtons[7].getDrawable() == frijol) {
                parametrosGanador(1);
            } else if (imageButtons[8].getDrawable() == frijol && imageButtons[9].getDrawable() == frijol && imageButtons[10].getDrawable() == frijol && imageButtons[11].getDrawable() == frijol) {
                parametrosGanador(2);
            } else if (imageButtons[12].getDrawable() == frijol && imageButtons[13].getDrawable() == frijol && imageButtons[14].getDrawable() == frijol && imageButtons[15].getDrawable() == frijol) {
                parametrosGanador(3);
            } else if (imageButtons[0].getDrawable() == frijol && imageButtons[4].getDrawable() == frijol && imageButtons[8].getDrawable() == frijol && imageButtons[12].getDrawable() == frijol) {
                parametrosGanador(4);
            } else if (imageButtons[1].getDrawable() == frijol && imageButtons[5].getDrawable() == frijol && imageButtons[9].getDrawable() == frijol && imageButtons[13].getDrawable() == frijol) {
                parametrosGanador(5);
            } else if (imageButtons[2].getDrawable() == frijol && imageButtons[6].getDrawable() == frijol && imageButtons[10].getDrawable() == frijol && imageButtons[14].getDrawable() == frijol) {
                parametrosGanador(6);
            } else if (imageButtons[3].getDrawable() == frijol && imageButtons[7].getDrawable() == frijol && imageButtons[11].getDrawable() == frijol && imageButtons[15].getDrawable() == frijol) {
                parametrosGanador(7);
            } else if (imageButtons[0].getDrawable() == frijol && imageButtons[5].getDrawable() == frijol && imageButtons[10].getDrawable() == frijol && imageButtons[15].getDrawable() == frijol) {
                parametrosGanador(8);
            } else if (imageButtons[3].getDrawable() == frijol && imageButtons[6].getDrawable() == frijol && imageButtons[9].getDrawable() == frijol && imageButtons[12].getDrawable() == frijol) {
                parametrosGanador(9);
            }
        } else {
            for (int i = 0; i < 16; i++) {
                if (imageButtons[i].getDrawable() != null) {
                    if (!Utils.comparadorImagenes(imageButtons[i].getDrawable(), frijol)) {
                        i = 16;
                    } else if (i == 15) {
                        if (ok != 0) soundPool.play(ok, 1, 1, 0, 0, 1);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dialogJugadorGanador        = new DialogJugadorGanador().recibiendoMsj(id_del_avatar);
                                dialogJugadorGanador.show(fragmentManager, "Mensaje");
                            }
                        }, 2000);

                        for (int g = 0; g < imageButtons.length - 1; g++) {
                            imageButtons[g].setBackgroundColor(res.getColor(R.color.rojo_dos));
                            imageButtons[g].startAnimation(parpadeo);
                        }
                        imageButtons[15].setBackgroundColor(res.getColor(R.color.rojo_dos));
                        imageButtons[15].startAnimation(parpadeo);
                        Utils.Konfetty(konfettiView);
                    }
                } else {
                    i = 16;
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (barajearPrimero.getVisibility() != View.VISIBLE){
                confirmarSalida.show(getFragmentManager(), "tag");
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}