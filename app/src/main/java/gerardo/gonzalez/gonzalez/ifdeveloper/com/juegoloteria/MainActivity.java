package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.activitys.ActivityEscogerTablero;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.activitys.ActivityGriton;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs.DialogAbout;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewFlipper viewFlipper;
    private LinearLayout ll;
    private ImageView llGriton, llNormal, instrucciones, about;
    private Animation entrada;
    private static final int intervalo = 2000;
    private long tiempoPrimerClick;
    private DialogAbout ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //FULLSCREEN
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        ab              = new DialogAbout();
        entrada         = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
        llGriton        = findViewById(R.id.open_griton);
        about           = findViewById(R.id.about);
        llNormal        = findViewById(R.id.open_normal);
        instrucciones   = findViewById(R.id.instrucciones);
        viewFlipper     = findViewById(R.id.image_paralax_inicio);
        ll              = findViewById(R.id.ll);

        ll.startAnimation(entrada);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right));
        viewFlipper.startFlipping();

        llGriton.setOnClickListener(this);
        llNormal.setOnClickListener(this);
        instrucciones.setOnClickListener(this);
        about.setOnClickListener(this);
    }

    private void mostrarAbout() {
        ab.show(getFragmentManager(), "about");
        Utils.RemoverFragment("about", getFragmentManager());
    }

    @Override
    public void onBackPressed() {
        if (tiempoPrimerClick + intervalo > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, R.string.toast_salir, Toast.LENGTH_SHORT).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.open_griton:
                startActivity(new Intent(getApplicationContext(), ActivityGriton.class));
                break;
            case R.id.open_normal:
                startActivity(new Intent(getApplicationContext(), ActivityEscogerTablero.class));
                break;
            case R.id.instrucciones:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = getResources().getString(R.string.url_app);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.main_activity_compartir)));
                break;
            case R.id.about:
                mostrarAbout();
                break;
        }
    }
}
