package gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.R;
import gerardo.gonzalez.gonzalez.ifdeveloper.com.juegoloteria.utils.Utils;

public class DialogJuegoTerminadoGriton extends DialogFragment{
    private View v;
    private ImageView btnAceptar;
    private TextView txtAceptar;
    private View.OnClickListener onClickListener;

    public DialogJuegoTerminadoGriton setTarjetasTermiadas (View.OnClickListener onClickListener){
     this.onClickListener = onClickListener;
     return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_griton_terminado, container, false);

        txtAceptar          = v.findViewById(R.id.txt_dialogo_griton_mensaje);
        btnAceptar          = v.findViewById(R.id.btn_griton_aceptar);

        txtAceptar.setTypeface(Utils.fuentesita(getActivity()));
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(view);
                dismiss();
            }
        });
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Objects.requireNonNull(dialog.getWindow()).requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }
}